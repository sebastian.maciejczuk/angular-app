import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, takeUntil, tap } from 'rxjs';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
})
export class NavigationComponent implements OnInit, OnDestroy {
  constructor(private router: Router, private readonly loginService: LoginService) {}

  private destroy$: Subject<void> = new Subject();

  public isAuthenticated: boolean;

  navigateToProducts() {
    this.router.navigateByUrl('/products');
  }

  navigateToLogin() {
    this.router.navigateByUrl('/login');
  }

  logout(): void {
    this.loginService.logout();
  }

  ngOnInit(): void {
    this.loginService.isAuthenticatedObserver$
      .pipe(
        tap((isAuthenticated) => (this.isAuthenticated = isAuthenticated)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
