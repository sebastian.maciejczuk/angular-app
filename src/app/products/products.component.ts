import { Component, OnDestroy, OnInit } from '@angular/core';
import { combineLatest, map, Subject, take, takeUntil, tap } from 'rxjs';
import { Product, ProductFilters } from '../models/product';
import { LoginService } from '../services/login.service';
import { ProductsApiService } from '../services/products-api.service';
import { ProductsService } from './products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit, OnDestroy {
  constructor(
    private readonly productsApiService: ProductsApiService,
    private readonly productsService: ProductsService,
    private readonly loginService: LoginService
  ) {
    this.loginService.isAuthenticatedObserver$
      .pipe(
        tap((value) => (this.isAuthenticated = value)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  public isAuthenticated: boolean;

  public filteredProductsList: Product[] = [];
  public products: Product[] = [];

  private destroy$: Subject<void> = new Subject();

  ngOnInit(): void {
    this.productsApiService.getProducts().pipe(take(1)).subscribe();

    combineLatest([this.productsApiService.products$.asObservable(), this.productsService.listFilters$.asObservable()])
      .pipe(
        map(([products, filters]) => this.filterList(products, filters)),
        tap((products) => (this.products = products)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  filterList(products: Product[], filters: ProductFilters): Product[] {
    return products.filter(({ name, type }) => {
      if (filters.type !== null) {
        return name.toLowerCase().includes(filters.search.toLowerCase()) && type === filters.type;
      }

      return name.toLowerCase().includes(filters.search.toLowerCase());
    });
  }
}
