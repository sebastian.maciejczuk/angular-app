import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Product, ProductFilters } from '../models/product';
import { getProducts } from '../utils/products.utils';

@Injectable()
export class ProductsService {
  public listFilters$: BehaviorSubject<ProductFilters> = new BehaviorSubject({
    search: '',
    type: null,
  } as ProductFilters);

  constructor() {}

  getProducts(): Observable<Product[]> {
    return of(getProducts());
  }
}
