import { Component, Input, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject, takeUntil, tap } from 'rxjs';
import { Product } from 'src/app/models/product';
import { LoginService } from 'src/app/services/login.service';
import { AddModalComponent } from '../panel/modals/add-modal/add-modal.component';
import { RemoveModalComponent } from '../panel/modals/remove-modal/remove-modal.component';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
export class ProductDetailsComponent implements OnDestroy {
  constructor(public dialog: MatDialog, private readonly loginService: LoginService) {
    this.loginService.isAuthenticatedObserver$
      .pipe(
        tap((value) => (this.isAuthenticated = value)),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  public isDescriptionExpanded = false;
  public isAuthenticated: boolean;

  private destroy$: Subject<void> = new Subject();

  @Input()
  product: Product;

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  openEditModal() {
    const dialogRef = this.dialog;

    this.dialog.open(AddModalComponent, {
      data: {
        id: this.product.id,
        editMode: true,
        product: this.product,
        dialogRef: dialogRef,
      },
    });
  }

  openRemoveModal() {
    const dialogRef = this.dialog;

    this.dialog.open(RemoveModalComponent, {
      data: {
        id: this.product.id,
        dialogRef: dialogRef,
      },
    });
  }

  handleDescription() {
    this.isDescriptionExpanded = !this.isDescriptionExpanded;
  }
}
