import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { debounceTime, Subject, takeUntil } from 'rxjs';
import { ProductFilterType, ProductType } from 'src/app/models/product';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-products-list-header',
  templateUrl: './products-list-header.component.html',
  styleUrls: ['./products-list-header.component.scss'],
})
export class ProductsListHeaderComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject();

  public productTypes: ProductFilterType[] = [
    {
      label: 'None',
      value: null,
    },
    {
      label: 'Advance',
      value: ProductType.advance,
    },
    {
      label: 'Basic',
      value: ProductType.basic,
    },
  ];

  public form: FormGroup = this.formBuilder.group({
    search: [''],
    type: [null],
  });

  constructor(private readonly formBuilder: FormBuilder, private readonly productsService: ProductsService) {}

  ngOnInit(): void {
    this.form?.valueChanges
      .pipe(debounceTime(200), takeUntil(this.destroy$))
      .subscribe((value) => this.productsService.listFilters$.next(value));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
