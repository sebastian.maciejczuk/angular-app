import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { ProductsListHeaderComponent } from './products-list-header/products-list-header.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { ProductsService } from './products.service';
import { PanelComponent } from './panel/panel.component';
import { AddModalComponent } from './panel/modals/add-modal/add-modal.component';
import { RemoveModalComponent } from './panel/modals/remove-modal/remove-modal.component';

const ProductsRoutes = [
  {
    path: '',
    component: ProductsComponent,
  },
];

@NgModule({
  declarations: [
    ProductsComponent,
    ProductDetailsComponent,
    ProductsListComponent,
    ProductsListHeaderComponent,
    PanelComponent,
    AddModalComponent,
    RemoveModalComponent,
  ],
  imports: [CommonModule, RouterModule.forChild(ProductsRoutes), SharedModule],
  providers: [ProductsService],
})
export class ProductsModule {}
