import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { faker } from '@faker-js/faker';
import { Subject, takeUntil } from 'rxjs';
import { Product, ProductFilterType, ProductType } from 'src/app/models/product';
import { ProductsApiService } from 'src/app/services/products-api.service';

interface AddModalProps {
  id?: string;
  product?: Product;
  dialogRef: MatDialog;
  editMode: boolean;
}

@Component({
  selector: 'app-add-modal',
  templateUrl: './add-modal.component.html',
  styleUrls: ['./add-modal.component.scss'],
})
export class AddModalComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject();

  public editMode = false;

  public product: {
    name: string;
    description: string;
    type: ProductType;
    imageURL: string;
  };

  public form: FormGroup = this.formBuilder.group({
    name: ['', Validators.required],
    description: ['', Validators.required],
    type: [null, Validators.required],
    imageURL: ['', Validators.required],
  });

  public productTypes: ProductFilterType[] = [
    {
      label: 'Advance',
      value: ProductType.advance,
    },
    {
      label: 'Basic',
      value: ProductType.basic,
    },
  ];

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly productsApiService: ProductsApiService,
    @Inject(MAT_DIALOG_DATA) public addModalProps: AddModalProps
  ) {}

  ngOnInit(): void {
    this.form?.valueChanges.pipe(takeUntil(this.destroy$)).subscribe((value) => (this.product = value));

    if (this.addModalProps.editMode) {
      this.editMode = true;

      const { description, name, imageURL, type } = this.addModalProps.product;

      this.form.setValue({ name: name, description: description, type: type, imageURL: imageURL });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onSubmit(): void {
    if (!this.editMode) {
      const newProduct: Product = {
        id: faker.datatype.uuid(),
        ...this.product,
      };

      const newData = [newProduct, ...this.productsApiService.products$.value];

      this.productsApiService.products$.next(newData);
    } else {
      const existingProductId = this.addModalProps.product.id;

      const newProduct: Product = {
        id: existingProductId,
        ...this.product,
      };

      this.productsApiService.products$.next([
        ...this.productsApiService.products$.value.map((product) =>
          product.id === existingProductId ? newProduct : product
        ),
      ]);
    }

    return this.addModalProps.dialogRef.closeAll();
  }
}
