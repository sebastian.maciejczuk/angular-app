import { Component, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProductsApiService } from 'src/app/services/products-api.service';

interface RemoveModalData {
  id: string;
  dialogRef: MatDialog;
}

@Component({
  selector: 'app-remove-modal',
  templateUrl: './remove-modal.component.html',
  styleUrls: ['./remove-modal.component.scss'],
})
export class RemoveModalComponent {
  productId: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: RemoveModalData,
    private readonly productsApiService: ProductsApiService
  ) {
    this.productId = data.id;
  }

  handleRemove() {
    const products = this.productsApiService.products$.value;

    this.productsApiService.products$.next([...products.filter(({ id }) => id !== this.productId)]);

    return this.data.dialogRef.closeAll();
  }
}
