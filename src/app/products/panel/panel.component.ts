import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddModalComponent } from './modals/add-modal/add-modal.component';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss'],
})
export class PanelComponent implements OnInit {
  constructor(public dialog: MatDialog) {}

  openModal() {
    const dialogRef = this.dialog;

    this.dialog.open(AddModalComponent, { data: { editMode: false, dialogRef: dialogRef } });
  }

  ngOnInit(): void {}
}
