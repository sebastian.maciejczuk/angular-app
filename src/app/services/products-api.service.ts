import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, tap } from 'rxjs';
import { Product } from '../models/product';
import { getProducts } from '../utils/products.utils';

@Injectable({
  providedIn: 'root',
})
export class ProductsApiService {
  public products$: BehaviorSubject<Product[]> = new BehaviorSubject([]);

  constructor() {}

  getProducts(): Observable<Product[]> {
    return of(getProducts()).pipe(tap((products) => this.products$.next(products)));
  }
}
