import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { user } from '../login/login.component';

@Injectable({
  providedIn: 'root',
})
export class LoginService implements OnInit {
  isAuthenticated: boolean;
  isAuthenticatedObserver$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private router: Router) {}

  login(user: user): void {
    if (user.password === 'admin' && user.username === 'admin') {
      this.isAuthenticatedObserver$.next(true);
      this.router.navigateByUrl('/products');
    }
  }

  logout() {
    this.isAuthenticatedObserver$.next(false);
    this.router.navigateByUrl('/login');
  }

  ngOnDestroy(): void {
    this.isAuthenticatedObserver$.complete();
  }

  ngOnInit(): void {
    this.isAuthenticatedObserver$.subscribe((value) => (this.isAuthenticated = value));
  }
}
