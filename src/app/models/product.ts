export enum ProductType {
  basic,
  advance,
}

export interface Product {
  id: string;
  name: string;
  description: string;
  imageURL: string;
  prices?: {
    EUR: string;
    PLN: string;
    USD: string;
  };
  type: ProductType;
}

export interface ProductFilterType {
  label: string;
  value: ProductType;
}

export interface ProductFilters {
  search?: string;
  type?: ProductType;
}
