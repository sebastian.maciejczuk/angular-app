import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortenDescription',
})
export class ShortenDescriptionPipe implements PipeTransform {
  transform(description: string, maxDescriptionLength = 120, isDescriptionExpanded = false): string {
    if (!isDescriptionExpanded && description.length > maxDescriptionLength) {
      return `${description.substring(0, maxDescriptionLength)}...`;
    }

    return description;
  }
}
