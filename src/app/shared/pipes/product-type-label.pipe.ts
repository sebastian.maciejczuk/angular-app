import { Pipe, PipeTransform } from '@angular/core';
import { ProductType } from 'src/app/models/product';

@Pipe({
  name: 'productTypeLabel',
})
export class ProductTypeLabelPipe implements PipeTransform {
  transform(value: ProductType): string {
    return value === ProductType.advance ? 'Advance' : 'Basic';
  }
}
