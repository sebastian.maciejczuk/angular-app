import { faker } from '@faker-js/faker';
import { Product, ProductType } from '../models/product';

export function createRandomProduct(i: number): Product {
  return {
    id: faker.datatype.uuid(),
    name: faker.random.words(3),
    description: faker.random.words(200),
    imageURL: faker.image.imageUrl(300, 200, 'cars', true),
    prices: {
      EUR: faker.random.numeric(2),
      PLN: faker.random.numeric(2),
      USD: faker.random.numeric(2),
    },
    type: i % 2 ? ProductType.basic : ProductType.advance,
  };
}

export const getProducts = () => Array.from({ length: 50 }).map((_, index: number) => createRandomProduct(index));
